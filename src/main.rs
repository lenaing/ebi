use std::time::{Duration, Instant, SystemTime};

use clap::{App, Arg};

mod chip8;
mod frontends;

/// Default Frames to display Per Second rate
const EBI_DEFAULT_FPS: u32 = 60;
/// Default Frames to display Per Second rate as slice
const EBI_DEFAULT_FPS_STR: &'static str = "60";

/// Default Updates Per Second rate
const EBI_DEFAULT_UPS: u32 = 700;
/// Default Updates Per Second rate as slice
const EBI_DEFAULT_UPS_STR: &'static str = "700";

/// V-Sync default
const EBI_DEFAULT_VSYNC: bool = false;
/// V-Sync default as slice
const EBI_DEFAULT_VSYNC_STR: &'static str = "false";

/// The `Interpreter` embeds both the CHIP-8 virtual machine and the GUI frontend.
///
/// It manages the main loop:
/// - Handling any user input
/// - Update the VM state at a constant given rate
/// - Renders the VM Video Memory as fast as possible
struct Interpreter {
    /// CHIP-8 Virtual Machine
    vm: chip8::vm::VM,
    /// GUI Frontend
    frontend: Box<dyn frontends::Frontend>,
    /// Current Frames Per Second rate
    fps: u32,
    /// Current Updates Per Second rate
    ups: u32,
}

impl Interpreter {
    /// Return current second from UNIX epoch start
    fn current_second(&self) -> u64 {
        SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs()
    }

    /// Main loop
    pub fn run(&mut self) {
        let mut last_second = self.current_second();

        let mut previous_frame_start = Instant::now();
        let mut update_lag = Duration::new(0, 0);
        let mut timers_lag = Duration::new(0, 0);

        let mut update_count = 0;
        let mut frame_count = 0;

        'running: loop {
            let frame_start = Instant::now();
            let elapsed = previous_frame_start.elapsed();

            // Handle input
            if !self.frontend.handle_input(&mut self.vm, &mut self.ups) {
                break 'running;
            }

            // As SDL2 ShowMessageBox freezes the thread,
            // do not count input handling in elapsed time
            previous_frame_start = Instant::now();

            update_lag += elapsed;
            timers_lag += elapsed;

            if self.vm.is_running() {
                // Update at required UPS
                let ms_per_update = Duration::new(0, 1000000000 as u32 / self.ups);
                while update_lag >= ms_per_update {
                    self.vm.step();
                    update_count += 1;
                    update_lag -= ms_per_update;
                }

                // Timers _must_ tick at 60Hz
                let ms_per_timers = Duration::new(0, 1000000000 as u32 / 60);
                while timers_lag >= ms_per_timers {
                    self.vm.step_60hz(&mut *self.frontend);
                    timers_lag -= ms_per_timers;
                }
            }

            // Render
            self.frontend.render(self.vm.memory.vram);
            frame_count += 1;

            // Update frontend with FPS and UPS infos
            let current_second = self.current_second();
            if current_second != last_second {
                self.frontend
                    .infos(&self.vm.rom_title(), frame_count, update_count);
                update_count = 0;
                frame_count = 0;
                last_second = current_second;
            }

            // If we are FPS capped we may have some time to sleep
            if self.fps > 0 {
                let ms_per_frame = Duration::new(0, 1000000000 as u32 / self.fps);
                let elapsed_time_since_frame_start = frame_start.elapsed();
                if elapsed_time_since_frame_start < ms_per_frame {
                    std::thread::sleep(ms_per_frame - elapsed_time_since_frame_start);
                }
            }
        }
    }
}

fn main() {
    let ebi = env!("CARGO_BIN_NAME");
    let ebi_version = env!("CARGO_PKG_VERSION");
    // Parse command line arguments
    let args = App::new(ebi)
        .version(ebi_version)
        .author("Étienne 'lenain' Girondel <lenaing@gmail.com>")
        .about("A CHIP-8 Interpreter")
        .long_about(
            "A CHIP-8 Interpreter

Pass a rom as argument to this program to load and run it.
You can also drag and drop a rom into ebi's window.

Keyboard bindings (French):

\t1 2 3 C  ->  & é \" '
\t4 5 6 D  ->  a z e r
\t7 8 9 E  ->  q s d f
\tA 0 B F  ->  w x c v

 Numeric Keypad + : UPS += 100
 Numeric Keypad - : UPS -= 100
 Esc:               Quit
",
        )
        .arg(
            Arg::with_name("ups")
                .takes_value(true)
                .short("u")
                .long("ups")
                .default_value(EBI_DEFAULT_UPS_STR)
                .help("Sets the target Updates Per Second rate."),
        )
        .arg(
            Arg::with_name("fps")
                .takes_value(true)
                .short("fps")
                .long("fps")
                .default_value(EBI_DEFAULT_FPS_STR)
                .help("Sets the target Frames Per Second rate. Set to 0 to uncap."),
        )
        .arg(
            Arg::with_name("vsync")
                .takes_value(true)
                .short("v")
                .long("vsync")
                .default_value(EBI_DEFAULT_VSYNC_STR)
                .possible_values(&["true", "false"])
                .help("Synchronize rendering to the monitor refresh rate."),
        )
        .arg(
            Arg::with_name("cpu-quirk-load-store")
                .long("cpu-quirk-load-store")
                .default_value("false")
                .possible_values(&["true", "false"])
                .help("Compatibility flag.")
                .long_help(
                    "Instructions 0xFX55 and 0xFX65 increments value of I register but some \
                CHIP-8 programs assume that they don't. Enabling this quirk causes I register to \
                remain unchanged after the instruction.",
                ),
        )
        .arg(
            Arg::with_name("cpu-quirk-shift")
                .long("cpu-quirk-shift")
                .default_value("false")
                .possible_values(&["true", "false"])
                .help("Compatibility flag.")
                .long_help(
                    "Instructions 8XY6 and 8XYE shift the value in the register VY and \
                store the result in VX but some CHIP-8 programs ignore VY, and simply shift VX. \
                Enabling this quirk causes VX to be shifted in place.",
                ),
        )
        .arg(Arg::with_name("rom").help("Sets the rom to load and run."))
        .get_matches();

    // Handle UPS argument
    let ups = match args.value_of("ups") {
        Some(ups) => match ups.parse::<u32>() {
            Ok(ups) => ups,
            Err(_) => {
                panic!("Invalid value for ups: {}", ups);
            }
        },
        None => EBI_DEFAULT_UPS,
    };

    // Handle FPS argument
    let mut fps = match args.value_of("fps") {
        Some(fps) => match fps.parse::<u32>() {
            Ok(fps) => fps,
            Err(_) => {
                panic!("Invalid value for fps: {}", fps);
            }
        },
        None => EBI_DEFAULT_FPS,
    };

    // Handle VSync argument
    let vsync = match args.value_of("vsync") {
        Some(vsync) => match vsync.parse::<bool>() {
            Ok(vsync) => {
                if vsync {
                    fps = 0;
                }
                vsync
            }
            Err(_) => {
                panic!("Invalid value for vsync: {}", vsync);
            }
        },
        None => EBI_DEFAULT_VSYNC,
    };

    // Start Interpreter
    println!("🦐 {} - CHIP-8 Interpreter - version {}", ebi, ebi_version);
    println!("Target UPS: {}", ups);
    if vsync {
        println!("VSync enabled");
    } else {
        match fps {
            0 => println!("Uncapped FPS"),
            fps => println!("Target FPS: {}", fps),
        }
    }

    let frontend = frontends::SDL2Frontend::new(vsync);
    let mut vm = chip8::vm::VM::new();

    // Handle CHIP-8 CPU Quirks
    match args.value_of("cpu-quirk-load-store") {
        Some(quirk) => match quirk.parse::<bool>() {
            Ok(quirk) => vm.set_cpu_load_store_quirk(quirk),
            Err(_) => {
                panic!("Invalid value for CHIP-8 CPU Load Store quirk: {}", quirk);
            }
        },
        None => {}
    }
    match args.value_of("cpu-quirk-shift") {
        Some(quirk) => match quirk.parse::<bool>() {
            Ok(quirk) => vm.set_cpu_shift_quirk(quirk),
            Err(_) => {
                panic!("Invalid value for CHIP-8 CPU Shift quirk: {}", quirk);
            }
        },
        None => {}
    }

    // Handle Rom argument
    match args.value_of("rom") {
        Some(filename) => {
            if !vm.load_rom(filename) {
                std::process::exit(1);
            }
        }
        None => {}
    }

    // Let's rock!
    Interpreter {
        vm,
        frontend: Box::new(frontend),
        fps,
        ups,
    }
    .run();
}
