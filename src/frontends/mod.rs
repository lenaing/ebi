//! Frontends defines CHIP-8 Interpreter display component

mod sdl2_frontend;
pub use sdl2_frontend::SDL2Frontend;

use crate::chip8;

/// The `Frontend` trait define the required methods for a CHIP-8 GUI and sound feedback.
pub trait Frontend {
    /// Handle user input.
    fn handle_input(&mut self, vm: &mut chip8::vm::VM, ups: &mut u32) -> bool;

    /// Render the Video RAM state and display it to the user.
    fn render(&mut self, vram: [u8; crate::chip8::memory::VIDEO_MEMORY_SIZE]);

    /// Start or stop beeping.
    fn beep(&mut self, beeping: bool);

    /// Display some runtime statistics.
    fn infos(&mut self, rom_title: &str, fps: u32, ups: u32);
}
