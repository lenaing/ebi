//! An SDL2 Frontend without OpenGL

use sdl2::{
    event::Event,
    keyboard::Keycode,
    messagebox::{show_message_box, ButtonData, ClickedButton, MessageBoxButtonFlag},
    mixer::{Music, DEFAULT_CHANNELS, DEFAULT_FORMAT},
    pixels::{Color, PixelFormatEnum},
    rect::Point,
    render::Canvas,
    surface::Surface,
    video::Window,
};

use super::Frontend;
use crate::chip8;

/// An SDL2 Frontend without OpenGL
pub struct SDL2Frontend {
    /// Canvas on which to draw the CHIP-8 video memory contents
    canvas: Canvas<Window>,
    /// Pump of input events to handle
    event_pump: sdl2::EventPump,
    /// If the beeping sound is currently playing
    beep_playing: bool,
    /// Beeping sound data
    _beep: Music<'static>,
}

impl SDL2Frontend {
    pub fn new(use_vsync: bool) -> Self {
        // Initialize SDL 2
        let sdl_context = sdl2::init().unwrap();

        // Initialize Video Subsystem
        let video_subsystem = sdl_context.video().unwrap();

        // Initialize SDL Mixer
        sdl2::mixer::open_audio(44_100, DEFAULT_FORMAT, DEFAULT_CHANNELS, 2048).unwrap();
        sdl2::mixer::init(sdl2::mixer::InitFlag::OGG).unwrap();

        // Embed beeping sound in the executable, load it at runtime
        let beep_data = include_bytes!("../../resources/beep.ogg");
        let beep = sdl2::mixer::Music::from_static_bytes(beep_data).unwrap();
        beep.play(-1).unwrap();
        sdl2::mixer::Music::pause();

        // Create main Window
        let mut window = video_subsystem
            .window("ebi", 640, 320)
            .position_centered()
            .build()
            .unwrap();

        // Embed icon in the executable, load it at runtime.
        // [Shrimp Icon](https://www.flaticon.com/free-icon/shrimp_2346897) made by
        // [Freepik]("https://www.freepik.com") from [Flaticon]("https://www.flaticon.com/").
        let icon_data = include_bytes!("../../resources/shrimp.data");
        let mut icon_data = icon_data.to_vec();
        let window_icon =
            Surface::from_data(icon_data.as_mut(), 64, 64, 256, PixelFormatEnum::RGBA32).unwrap();
        window.set_icon(window_icon);

        // Create canvas
        let mut canvas = if use_vsync {
            window.into_canvas().present_vsync().build().unwrap()
        } else {
            window.into_canvas().build().unwrap()
        };
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.set_scale(10.0, 10.0).unwrap();
        canvas.clear();
        canvas.present();

        // Grab event pump
        let event_pump = sdl_context.event_pump().unwrap();

        println!("SDL2 Frontend running on {}", sdl2::get_platform());
        println!(
            "SDL2 Version: {} - {} ({})",
            sdl2::version::version(),
            sdl2::version::revision_number(),
            sdl2::version::revision()
        );
        println!("SDL2 Mixer: {}", sdl2::mixer::get_linked_version());

        Self {
            canvas,
            event_pump,
            beep_playing: false,
            _beep: beep,
        }
    }
}

impl Frontend for SDL2Frontend {
    fn handle_input(&mut self, vm: &mut chip8::vm::VM, ups: &mut u32) -> bool {
        let mut keep_running = true;
        let mut has_handled_quit_event = false;

        for event in self.event_pump.poll_iter() {
            match event {
                // Quit Interpreter
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => {
                    // Skip all other quit event
                    if has_handled_quit_event {
                        continue;
                    }
                    let buttons: Vec<_> = vec![
                        ButtonData {
                            flags: MessageBoxButtonFlag::NOTHING,
                            button_id: 1,
                            text: "Quit",
                        },
                        ButtonData {
                            flags: MessageBoxButtonFlag::ESCAPEKEY_DEFAULT
                                | MessageBoxButtonFlag::RETURNKEY_DEFAULT,
                            button_id: 2,
                            text: "Cancel",
                        },
                    ];
                    let ebi = env!("CARGO_BIN_NAME");
                    let answer = show_message_box(
                        sdl2::messagebox::MessageBoxFlag::WARNING,
                        &buttons,
                        ebi,
                        &format!("Are you sure you want to quit {}?", ebi),
                        None,
                        None,
                    )
                    .unwrap();
                    match answer {
                        ClickedButton::CustomButton(ButtonData { button_id: 1, .. }) => {
                            keep_running = false;
                        }
                        _ => {}
                    }
                    has_handled_quit_event = true;
                }

                // Update UPS rate
                Event::KeyDown {
                    keycode: Some(Keycode::KpPlus),
                    ..
                } => {
                    *ups += 100;
                }
                Event::KeyDown {
                    keycode: Some(Keycode::KpMinus),
                    ..
                } => {
                    if *ups > 100 {
                        *ups -= 100;
                    }
                }

                // Load ROM on Drag & Drop
                Event::DropFile { filename, .. } => {
                    vm.load_rom(&filename);
                }

                // CHIP-8 Keyboard Handling
                Event::KeyDown {
                    keycode: Some(key), ..
                } => {
                    let key_press = match key {
                        Keycode::Num1 => Some(1),
                        Keycode::Num2 => Some(2),
                        Keycode::Num3 => Some(3),
                        Keycode::Num4 => Some(0xC),
                        Keycode::A => Some(4),
                        Keycode::Z => Some(5),
                        Keycode::E => Some(6),
                        Keycode::R => Some(0xD),
                        Keycode::Q => Some(0x7),
                        Keycode::S => Some(0x8),
                        Keycode::D => Some(0x9),
                        Keycode::F => Some(0xE),
                        Keycode::W => Some(0xA),
                        Keycode::X => Some(0x0),
                        Keycode::C => Some(0xB),
                        Keycode::V => Some(0xF),
                        _ => None,
                    };
                    if let Some(key) = key_press {
                        vm.keyboard.press(key);
                    }
                }
                Event::KeyUp {
                    keycode: Some(key), ..
                } => {
                    let key_press = match key {
                        Keycode::Num1 => Some(1),
                        Keycode::Num2 => Some(2),
                        Keycode::Num3 => Some(3),
                        Keycode::Num4 => Some(0xC),
                        Keycode::A => Some(4),
                        Keycode::Z => Some(5),
                        Keycode::E => Some(6),
                        Keycode::R => Some(0xD),
                        Keycode::Q => Some(0x7),
                        Keycode::S => Some(0x8),
                        Keycode::D => Some(0x9),
                        Keycode::F => Some(0xE),
                        Keycode::W => Some(0xA),
                        Keycode::X => Some(0x0),
                        Keycode::C => Some(0xB),
                        Keycode::V => Some(0xF),
                        _ => None,
                    };
                    if let Some(key) = key_press {
                        vm.keyboard.release(key);
                    }
                }
                _ => {}
            }
        }
        keep_running
    }

    fn render(&mut self, vram: [u8; crate::chip8::memory::VIDEO_MEMORY_SIZE]) {
        self.canvas.clear();
        for x in 0..64 {
            for y in 0..32 {
                if vram[x + y * 64] != 0 {
                    self.canvas.set_draw_color(Color::RGB(0xfc, 0x53, 0x27));
                } else {
                    self.canvas.set_draw_color(Color::RGB(0x00, 0x00, 0x00));
                }
                self.canvas
                    .draw_point(Point::new(x as i32, y as i32))
                    .unwrap();
            }
        }
        self.canvas.present();
    }

    fn infos(&mut self, rom_title: &str, fps: u32, ups: u32) {
        let ebi = env!("CARGO_BIN_NAME");
        let title = if rom_title.is_empty() {
            ebi.to_string()
        } else {
            format!("{} - {} - {} UPS / {} FPS", ebi, rom_title, ups, fps)
        };
        self.canvas.window_mut().set_title(&title).unwrap();
    }

    fn beep(&mut self, beeping: bool) {
        if beeping {
            if !self.beep_playing {
                sdl2::mixer::Music::resume();
                self.beep_playing = true;
            }
        } else {
            if self.beep_playing {
                sdl2::mixer::Music::pause();
                self.beep_playing = false;
            }
        }
    }
}
