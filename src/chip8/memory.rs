//! CHIP-8 Memory

/// CHIP-8 Memory size
pub const MEMORY_SIZE: usize = 4096;
/// CHIP-8 Video Memory size
pub const VIDEO_MEMORY_SIZE: usize = 64 * 32;
/// CHIP-8 Interpreter length
pub const CHIP8_INTERPRETER_LENGTH: usize = 512;
/// CHIP-8 Display Refresh reserved byte count
pub const CHIP8_DISPLAY_RESERVED: usize = 256;
/// CHIP-8 Call stack, internal use, and other variables reserved byte count
pub const CHIP8_MISC_RESERVED: usize = 96;

/// CHIP-8 Memory
pub struct Memory {
    /// 4K RAM
    pub ram: [u8; MEMORY_SIZE],
    /// 1-bit 64x32 pixels display
    pub vram: [u8; VIDEO_MEMORY_SIZE],
}

impl Memory {
    pub fn new() -> Self {
        Self {
            ram: [0; MEMORY_SIZE],
            vram: [0; VIDEO_MEMORY_SIZE],
        }
    }

    /// Reset RAM, VRAM and load font set.
    pub fn reset(&mut self) {
        self.ram = [0; MEMORY_SIZE];
        self.vram = [0; VIDEO_MEMORY_SIZE];
        let font_set = [
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80, // F
        ];

        for i in 0..font_set.len() {
            self.ram[i] = font_set[i];
        }
    }

    /// Read from RAM
    pub fn read(&self, address: usize) -> u16 {
        self.ram[address] as u16
    }

    /// Write to RAM
    pub fn write(&mut self, address: usize, data: u8) {
        self.ram[address] = data;
    }

    /// Read from VRAM
    pub fn vram_read(&self, address: usize) -> u16 {
        self.vram[address] as u16
    }

    /// Read to VRAM
    pub fn vram_write(&mut self, address: usize, data: u8) {
        self.vram[address] = data;
    }
}
