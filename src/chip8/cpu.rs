//! CHIP-8 CPU
use crate::frontends::Frontend;

use super::{keyboard, memory};

/// CHIP-8 CPU
pub struct CPU {
    /// 16-bit Program Counter
    pc: u16,
    /// 16 8-bit registers
    v: [u8; 16],
    /// 16-bit Address register
    i: u16,
    /// Stack
    stack: [u16; 12],
    /// Stack Pointer
    sp: usize,
    /// Sound Timer
    sound: u8,
    /// Delay Timer
    delay: u8,
    /// Load/Store Quirk
    pub load_store_quirk: bool,
    /// Shift Quirk
    pub shift_quirk: bool,
}

/// CHIP-8 Opcodes
#[derive(PartialEq)]
enum Opcode {
    Unknown,
    Op0NNN { address: u16 },
    Op00E0,
    Op00EE,
    Op1NNN { address: u16 },
    Op2NNN { address: u16 },
    Op3XNN { vx: usize, byte: u8 },
    Op4XNN { vx: usize, byte: u8 },
    Op5XY0 { vx: usize, vy: usize },
    Op6XNN { vx: usize, byte: u8 },
    Op7XNN { vx: usize, byte: u8 },
    Op8XY0 { vx: usize, vy: usize },
    Op8XY1 { vx: usize, vy: usize },
    Op8XY2 { vx: usize, vy: usize },
    Op8XY3 { vx: usize, vy: usize },
    Op8XY4 { vx: usize, vy: usize },
    Op8XY5 { vx: usize, vy: usize },
    Op8XY6 { vx: usize, vy: usize },
    Op8XY7 { vx: usize, vy: usize },
    Op8XYE { vx: usize, vy: usize },
    Op9XY0 { vx: usize, vy: usize },
    OpANNN { address: u16 },
    OpBNNN { address: u16 },
    OpCXNN { vx: usize, byte: u8 },
    OpDXYN { vx: usize, vy: usize, height: usize },
    OpEX9E { vx: usize },
    OpEXA1 { vx: usize },
    OpFX07 { vx: usize },
    OpFX0A { vx: usize },
    OpFX15 { vx: usize },
    OpFX18 { vx: usize },
    OpFX1E { vx: usize },
    OpFX29 { vx: usize },
    OpFX33 { vx: usize },
    OpFX55 { vx: usize },
    OpFX65 { vx: usize },
}

impl CPU {
    pub fn new() -> Self {
        Self {
            pc: 0,
            i: 0,
            v: [0; 16],
            stack: [0; 12],
            sp: 0,
            sound: 0,
            delay: 0,
            load_store_quirk: false,
            shift_quirk: false,
        }
    }

    /// Reset the CPU program counter, registers, stack and timers
    pub fn reset(&mut self) {
        self.pc = memory::CHIP8_INTERPRETER_LENGTH as u16;
        self.i = 0;
        self.v = [0; 16];
        self.stack = [0; 12];
        self.sp = 0;
        self.sound = 0;
        self.delay = 0;
    }

    /// Fetch an instruction from memory
    fn fetch(&self, memory: &memory::Memory) -> u16 {
        memory.read(self.pc as usize) << 8 | memory.read(self.pc as usize + 1)
    }

    /// Decode an instruction
    fn decode(&mut self, opcode: u16) -> Opcode {
        let address = opcode & 0x0fff;
        let vx = ((opcode & 0xf00) >> 8) as usize;
        let vy = ((opcode & 0x0f0) >> 4) as usize;
        let byte = (opcode & 0x00ff) as u8;
        let nibble = (opcode & 0x000f) as i8;

        let decoded_opcode = match opcode & 0xf000 {
            0x0000 => match byte {
                0xE0 => Opcode::Op00E0,
                0xEE => Opcode::Op00EE,
                _ => Opcode::Op0NNN { address },
            },
            0x1000 => Opcode::Op1NNN { address },
            0x2000 => Opcode::Op2NNN { address },
            0x3000 => Opcode::Op3XNN { vx, byte },
            0x4000 => Opcode::Op4XNN { vx, byte },
            0x5000 => match nibble {
                0 => Opcode::Op5XY0 { vx, vy },
                _ => Opcode::Unknown,
            },
            0x6000 => Opcode::Op6XNN { vx, byte },
            0x7000 => Opcode::Op7XNN { vx, byte },
            0x8000 => match nibble {
                0 => Opcode::Op8XY0 { vx, vy },
                1 => Opcode::Op8XY1 { vx, vy },
                2 => Opcode::Op8XY2 { vx, vy },
                3 => Opcode::Op8XY3 { vx, vy },
                4 => Opcode::Op8XY4 { vx, vy },
                5 => Opcode::Op8XY5 { vx, vy },
                6 => Opcode::Op8XY6 { vx, vy },
                7 => Opcode::Op8XY7 { vx, vy },
                0xE => Opcode::Op8XYE { vx, vy },
                _ => Opcode::Unknown,
            },
            0x9000 => match nibble {
                0 => Opcode::Op9XY0 { vx, vy },
                _ => Opcode::Unknown,
            },
            0xa000 => Opcode::OpANNN { address },
            0xb000 => Opcode::OpBNNN { address },
            0xc000 => Opcode::OpCXNN { vx, byte },
            0xd000 => Opcode::OpDXYN {
                vx,
                vy,
                height: nibble as usize,
            },
            0xe000 => match byte {
                0x9E => Opcode::OpEX9E { vx },
                0xA1 => Opcode::OpEXA1 { vx },
                _ => Opcode::Unknown,
            },
            0xf000 => match byte {
                0x07 => Opcode::OpFX07 { vx },
                0x0A => Opcode::OpFX0A { vx },
                0x15 => Opcode::OpFX15 { vx },
                0x18 => Opcode::OpFX18 { vx },
                0x1E => Opcode::OpFX1E { vx },
                0x29 => Opcode::OpFX29 { vx },
                0x33 => Opcode::OpFX33 { vx },
                0x55 => Opcode::OpFX55 { vx },
                0x65 => Opcode::OpFX65 { vx },
                _ => Opcode::Unknown,
            },
            _ => Opcode::Unknown,
        };
        if decoded_opcode == Opcode::Unknown {
            panic!("Failed to decode Opcode 0x{:4X}", opcode);
        }
        self.pc += 2;
        decoded_opcode
    }

    /// Execute an instruction
    fn execute(
        &mut self,
        opcode: u16,
        decoded_opcode: Opcode,
        memory: &mut memory::Memory,
        keyboard: &keyboard::Keyboard,
    ) {
        match decoded_opcode {
            Opcode::Op00E0 => {
                for address in 0..(64 * 32) {
                    memory.vram_write(address, 0);
                }
            }
            Opcode::Op00EE => {
                self.sp -= 1;
                self.pc = self.stack[self.sp];
            }
            Opcode::Op1NNN { address } => {
                self.pc = address;
            }
            Opcode::Op2NNN { address } => {
                self.stack[self.sp] = self.pc;
                self.sp += 1;
                self.pc = address;
            }
            Opcode::Op3XNN { vx, byte } => {
                if self.v[vx] == byte {
                    self.pc += 2;
                }
            }
            Opcode::Op4XNN { vx, byte } => {
                if self.v[vx] != byte {
                    self.pc += 2;
                }
            }
            Opcode::Op5XY0 { vx, vy } => {
                if self.v[vx] == self.v[vy] {
                    self.pc += 2;
                }
            }
            Opcode::Op6XNN { vx, byte } => {
                self.v[vx] = byte;
            }
            Opcode::Op7XNN { vx, byte } => {
                self.v[vx] = self.v[vx].wrapping_add(byte);
            }
            Opcode::Op8XY0 { vx, vy } => {
                self.v[vx] = self.v[vy];
            }
            Opcode::Op8XY1 { vx, vy } => {
                self.v[vx] |= self.v[vy];
            }
            Opcode::Op8XY2 { vx, vy } => {
                self.v[vx] &= self.v[vy];
            }
            Opcode::Op8XY3 { vx, vy } => {
                self.v[vx] ^= self.v[vy];
            }
            Opcode::Op8XY4 { vx, vy } => {
                if self.v[vx] < self.v[vy] {
                    self.v[0xF] = 0;
                } else {
                    self.v[0xF] = 1;
                }

                self.v[vx] = self.v[vx].wrapping_add(self.v[vy]);
            }
            Opcode::Op8XY5 { vx, vy } => {
                self.v[0xF] = 1;
                if self.v[vx] < self.v[vy] {
                    self.v[0xF] = 0;
                }
                self.v[vx] = self.v[vx].wrapping_sub(self.v[vy]);
            }
            Opcode::Op8XY6 { vx, vy } => {
                // Original CHIP-8 shifted VY and stored the result in VX
                let v = if self.shift_quirk { vx } else { vy };
                self.v[0xF] = self.v[v] & 1;
                self.v[vx] = self.v[v] >> 1;
            }
            Opcode::Op8XY7 { vx, vy } => {
                self.v[0xF] = 1;
                if self.v[vx] > self.v[vy] {
                    self.v[0xF] = 0;
                }
                self.v[vx] = self.v[vy].wrapping_sub(self.v[vx]);
            }
            Opcode::Op8XYE { vx, vy } => {
                // Original CHIP-8 shifted VY and stored the result in VX
                let v = if self.shift_quirk { vx } else { vy };
                self.v[0xF] = self.v[v] >> 7;
                self.v[vx] = self.v[v] << 1;
            }
            Opcode::Op9XY0 { vx, vy } => {
                if self.v[vx] != self.v[vy] {
                    self.pc += 2;
                }
            }
            Opcode::OpANNN { address } => {
                self.i = address;
            }
            Opcode::OpBNNN { address } => {
                self.pc = address + self.v[0] as u16;
            }
            Opcode::OpCXNN { vx, byte } => {
                self.v[vx] = rand::random::<u8>() & byte;
            }
            Opcode::OpDXYN { vx, vy, height } => {
                let x = self.v[vx] as usize;
                let y = self.v[vy] as usize;

                self.v[0xF] = 0;

                for yline in 0..height {
                    let sprite_line = memory.read(self.i as usize + yline);

                    for xline in 0..8 {
                        if sprite_line & (0x80 >> xline) != 0 {
                            if y + yline <= 31 {
                                let address = (x + xline + (y + yline) * 64) % 2048;
                                let mut pixel = memory.vram_read(address) as u8;
                                if pixel != 0 {
                                    self.v[0xF] = 1;
                                }
                                pixel ^= 1;
                                memory.vram_write(address, pixel);
                            }
                        }
                    }
                }
            }
            Opcode::OpEX9E { vx } => {
                if keyboard.keys[self.v[vx] as usize] {
                    self.pc += 2;
                }
            }
            Opcode::OpEXA1 { vx } => {
                if !keyboard.keys[self.v[vx] as usize] {
                    self.pc += 2;
                }
            }
            Opcode::OpFX07 { vx } => {
                self.v[vx] = self.delay;
            }
            Opcode::OpFX0A { vx } => {
                let mut pressed = false;

                for i in 0..keyboard.keys.len() {
                    if keyboard.keys[i] {
                        self.v[vx] = i as u8;
                        pressed = true;
                    }
                }

                if !pressed {
                    self.pc -= 2;
                }
            }
            Opcode::OpFX15 { vx } => self.delay = self.v[vx] as u8,
            Opcode::OpFX18 { vx } => self.sound = self.v[vx] as u8,
            Opcode::OpFX29 { vx } => self.i = (self.v[vx] * 5) as u16,
            Opcode::OpFX1E { vx } => {
                self.i += self.v[vx] as u16;
            }
            Opcode::OpFX33 { vx } => {
                memory.write(self.i as usize, (self.v[vx] / 100) as u8);
                memory.write(self.i as usize + 1, ((self.v[vx] / 10) % 10) as u8);
                memory.write(self.i as usize + 2, ((self.v[vx] % 100) % 10) as u8);
            }
            Opcode::OpFX55 { vx } => {
                for i in 0..=vx {
                    memory.write(self.i as usize + i, self.v[i] as u8);
                }
                if !self.load_store_quirk {
                    // Original CHIP-8 increments I
                    self.i += (vx + 1) as u16;
                }
            }
            Opcode::OpFX65 { vx } => {
                for i in 0..=vx {
                    self.v[i] = memory.read(self.i as usize + i) as u8;
                }
                if !self.load_store_quirk {
                    // Original CHIP-8 increments I
                    self.i += (vx + 1) as u16;
                }
            }
            _ => unimplemented!("Failed to execute opcode 0x{:4X}", opcode),
        }
    }

    /// A full fetch / decode / execute cycle
    pub fn tick(&mut self, memory: &mut memory::Memory, keyboard: &keyboard::Keyboard) {
        let opcode = self.fetch(memory);
        let decoded = self.decode(opcode);
        self.execute(opcode, decoded, memory, keyboard);
    }

    /// Execute a timers' cycle at 60Hz
    pub fn tick_timers(&mut self, frontend: &mut dyn Frontend) {
        if self.delay > 0 {
            self.delay -= 1;
        }
        if self.sound > 0 {
            frontend.beep(true);
            self.sound -= 1;
        }
        if self.sound == 0 {
            frontend.beep(false);
        }
    }
}
