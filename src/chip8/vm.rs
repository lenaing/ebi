//! CHIP-8 Virtual Machine
use std::{fs::File, io::Read, path::Path};

use super::{cpu, keyboard, memory};
use crate::frontends::Frontend;

/// CHIP-8 Virtual Machine
pub struct VM {
    cpu: cpu::CPU,
    pub memory: memory::Memory,
    pub keyboard: keyboard::Keyboard,
    running: bool,
    rom_title: String,
}

impl VM {
    pub fn new() -> Self {
        let memory = memory::Memory::new();
        let keyboard = keyboard::Keyboard::new();
        let cpu = cpu::CPU::new();
        VM {
            cpu,
            memory,
            keyboard,
            running: false,
            rom_title: "".to_string(),
        }
    }

    /// Reset CPU & Memory
    fn reset(&mut self) {
        self.memory.reset();
        self.cpu.reset();
    }

    /// Reset, load a ROM to memory and resume execution
    pub fn load_rom(&mut self, filename: &str) -> bool {
        self.reset();

        print!("Loading rom '{}' ", filename);

        match File::open(filename) {
            Ok(mut rom) => {
                let mut buffer = Vec::new();
                if rom.read_to_end(&mut buffer).is_err() {
                    println!("KO. Failed to read file.");
                    return false;
                }

                if buffer.len()
                    > memory::MEMORY_SIZE
                        - memory::CHIP8_INTERPRETER_LENGTH
                        - memory::CHIP8_DISPLAY_RESERVED
                        - memory::CHIP8_MISC_RESERVED
                {
                    println!("KO. File too large to be a CHIP-8 program.");
                    return false;
                }

                for i in 0..buffer.len() {
                    self.memory
                        .write(memory::CHIP8_INTERPRETER_LENGTH + i, buffer[i]);
                }

                println!("OK");
            }
            Err(error) => {
                println!("KO. Failed to open file: {}.", error);
                return false;
            }
        };

        self.rom_title = Path::new(filename)
            .file_stem()
            .unwrap()
            .to_string_lossy()
            .to_string();

        self.resume();
        true
    }

    /// Run one CPU cycle
    pub fn step(&mut self) {
        self.cpu.tick(&mut self.memory, &self.keyboard);
    }

    /// Run one CPU timers' cycle
    pub fn step_60hz(&mut self, frontend: &mut dyn Frontend) {
        self.cpu.tick_timers(frontend);
    }

    /// Enable/Disable CPU Load/Store Quirk
    pub fn set_cpu_load_store_quirk(&mut self, enabled: bool) {
        self.cpu.load_store_quirk = enabled;
        println!(
            "CPU Load/Store Quirk: {}",
            if enabled { "enabled" } else { "disabled" }
        );
    }

    /// Enable/Disable CPU Shift Quirk
    pub fn set_cpu_shift_quirk(&mut self, enabled: bool) {
        self.cpu.shift_quirk = enabled;
        println!(
            "CPU Shift Quirk: {}",
            if enabled { "enabled" } else { "disabled" }
        );
    }

    /// Get title of the current ROM loaded in the VM
    pub fn rom_title(&self) -> String {
        self.rom_title.to_string()
    }

    /// Is currently executing instructions
    pub fn is_running(&self) -> bool {
        self.running
    }

    /// Resume execution
    fn resume(&mut self) {
        self.running = true
    }
}
