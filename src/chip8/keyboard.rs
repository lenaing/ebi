//! CHIP-8 Hex Keyboard

/// CHIP-8 Hex Keyboard
pub struct Keyboard {
    /// Hex Keyboard keys state
    pub keys: [bool; 16],
}

impl Keyboard {
    pub fn new() -> Self {
        Self { keys: [false; 16] }
    }

    /// Press a key on the Hex keyboard
    pub fn press(&mut self, key: usize) {
        self.keys[key] = true;
    }

    /// Release a key on the Hex keyboard
    pub fn release(&mut self, key: usize) {
        self.keys[key] = false;
    }
}
