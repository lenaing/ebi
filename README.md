# ebi
[![pipeline status](https://gitlab.com/lenaing/ebi/badges/master/pipeline.svg)](https://gitlab.com/lenaing/ebi/pipelines)

_ebi_ is a [COSMAC ELF](https://en.wikipedia.org/wiki/RCA_1802) [CHIP-8](https://en.wikipedia.org/wiki/CHIP-8) virtual machine interpreter written in [Rust](https://www.rust-lang.org/) with an [SDL 2](https://www.libsdl.org/) frontend.

<img src="resources/shrimp.png" width="64" />

## Fritures / Features

* Implements CHIP-8:
  * Full instruction set: ✓
  * Known CPU quirks: ✓
  * CPU Registers, Stack, Memory and font set: ✓
  * Monochrome 64x32 pixel display: ✓
  * Keyboard input handling: ✓
  * Timers handling: ✓
* Load and play ROMs: ✓
* Configurable/on-demand change of Updates per second: ✓
* Configurable Vsync or FPS (Capped or Uncapped): ✓
* Sound: ✓
* Drag & Drop ROM switch: ✓

Nice to have but #iAmLazy now:

* Standalone binary: ❌
* Stack Overflow check: ❌
* Better Sound: ❌
* Automatic CPU quirks activation based on known ROMs: ❌
* Configurable keyboard mapping: ❌
* Configurable color display: ❌
* Screen scaling: ❌
* CRT Filter: ❌
* Boot ROM: ❌
* Integrated debugger: ❌
  * Reset interpreter: ❌
  * Pause / Resume interpreting: ❌
  * Step over / into / out: ❌
  * Register values:
    * CLI ❌
    * GUI ❌
  * Instruction disassembly:
    * CLI ❌
    * GUI ❌
  * Set / Unset Breakpoints
* Super CHIP-8 support: ❌
* ETI-660 support: ❌

## Compatibility

* [corax89](https://github.com/corax89/chip8-test-rom)'s test rom: ✓
* [Skolusor](https://github.com/Skosulor/c8int/tree/master/test)'s test rom: ✓ (Needs CPU Shift Quirk)
* [bestcoder](https://twitter.com/BestCoder_)'s test rom: ✓ (Needs both CPU Shift Quirk and Load/Store Quirk)

## Quickstart

Start _ebi_ and wait for drag and drop of a ROM file to load and run it:

```shell
ebi
```

Run the _PONG_ game at 60 FPS and 700 Updates per second:

```shell
ebi games/PONG
```

Run the _INVADERS_ game at 30 FPS and enable the required CPU quirk:

```shell
ebi --cpu-quirk-shift true --fps 30 games/INVADERS
```

## Looking for ROMs ?

Here are some links to public domain CHIP-8 ROMs to play with:

- [CHIP-8 Community Archive](https://github.com/JohnEarnest/chip8Archive)
- [Zophar's Domain](https://www.zophar.net/pdroms/chip8/chip-8-games-pack.html)  ([Backup](https://archive.org/details/Chip-8RomsThatAreInThePublicDomain))

## Screenshots

<img src="resources/pong.png" />

<img src="resources/invaders.png" />

## Credits

[Shrimp Icon](https://www.flaticon.com/free-icon/shrimp_2346897) made by [Freepik]("https://www.freepik.com") from [Flaticon]("https://www.flaticon.com/").